package test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class GetBusinessCondition {

	public static void main(String[] args) throws Exception {
		StringBuilder urlBuilder = new StringBuilder("http://apis.data.go.kr/1230000/IndstrytyBaseLawrgltInfoService/getIndstrytyBaseLawrgltInfoList"); /*URL*/
	    urlBuilder.append("?" + URLEncoder.encode("ServiceKey","UTF-8") + "=S14Vx7VeTweRt4wUhkMFFXwWiKQofAxdN04Ojsiae3%2B7siHFHOATspMu4hzDk0ODP9Gzj1Jo6g2WUGKswnxcpw%3D%3D"); /*Service Key*/
	    urlBuilder.append("&" + URLEncoder.encode("numOfRows","UTF-8") + "=" + URLEncoder.encode("50", "UTF-8")); /*한 페이지 결과 수*/
	    urlBuilder.append("&" + URLEncoder.encode("pageNo","UTF-8") + "=" + URLEncoder.encode("1", "UTF-8")); /*페이지 번호*/
	    urlBuilder.append("&" + URLEncoder.encode("indstrytyClsfcCd","UTF-8") + "=" + URLEncoder.encode("", "UTF-8")); /*검색하고자하는 업종을 분류하는 코드*/
	    urlBuilder.append("&" + URLEncoder.encode("indstrytyNm","UTF-8") + "=" + URLEncoder.encode("", "UTF-8")); /*검색하고자하는 업종에 대한 분류 명*/
	    urlBuilder.append("&" + URLEncoder.encode("indstrytyCd","UTF-8") + "=" + URLEncoder.encode("722000", "UTF-8")); /*검색하고자하는 업종을 분류하는 코드 (코드값)*/
	    urlBuilder.append("&" + URLEncoder.encode("inqryBgnDt","UTF-8") + "=" + URLEncoder.encode("195001010000", "UTF-8")); /*검색하고자하는 업종등록일 기준 조회시작일시*/
	    urlBuilder.append("&" + URLEncoder.encode("inqryEndDt","UTF-8") + "=" + URLEncoder.encode("201601012359", "UTF-8")); /*검색하고자하는 업종등록일 기준 조회종료일시*/
	    URL url = new URL(urlBuilder.toString());
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Content-type", "application/json");
	    System.out.println("Response code: " + conn.getResponseCode());
	    BufferedReader rd;
	    if(conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
	        rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    } else {
	        rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
	    }
	    StringBuilder sb = new StringBuilder();
	    String line;
	    while ((line = rd.readLine()) != null) {
	        sb.append(line);
	    }
	    rd.close();
	    conn.disconnect();
	    System.out.println(sb.toString());
	    System.out.println(urlBuilder.toString());

	}

}

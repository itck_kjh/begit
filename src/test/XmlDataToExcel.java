package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XmlDataToExcel {

    public static void main(String argv[]){

        ArrayList<String> firstNames = new ArrayList<String>();
        ArrayList<String> lastNames = new ArrayList<String>();

        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
/*            Document doc = docBuilder.parse(new File("C:/Users/hussain.a/Desktop/book.xml"));*/
            Document doc = docBuilder.parse("http://apis.data.go.kr/1611000/nsdi/EstateBrkpgService/attr/getEBOfficeInfo?ldCode=11110&bsnmCmpnm=&brkrNm=&sttusSeCode=1&ServiceKey=2Ei%2FzEWtXC6pki%2BYHsuHkLwlwvkxqn0onlDd3gGfyvcj2HO3anA6CBVNxrxI7l09e0%2FWr4cpmsQHEEoOQp7qoQ%3D%3D");

            // normalize text representation
            doc.getDocumentElement().normalize();
            System.out.println("Root element of the doc is :\" "+ doc.getDocumentElement().getNodeName() + "\"");
            NodeList listOfPersons = doc.getElementsByTagName("field");
            int totalPersons = listOfPersons.getLength();
            System.out.println("Total no of item : " + totalPersons);
            System.out.println("길이"+listOfPersons.getLength());
            for (int s = 0; s < listOfPersons.getLength(); s++) 
            {
                Node firstPersonNode = listOfPersons.item(s);
                if (firstPersonNode.getNodeType() == Node.ELEMENT_NODE) 
                {
                	//얻고자하는 key값 추가 
                    Element firstPersonElement = (Element) firstPersonNode;
                    NodeList firstNameList = firstPersonElement.getElementsByTagName("bsnmCmpnm");
                    Element firstNameElement = (Element) firstNameList.item(0);
                    NodeList textFNList = firstNameElement.getChildNodes();
                    System.out.println("업소명 : "+ ((Node) textFNList.item(0)).getNodeValue().trim());
                    firstNames.add(((Node) textFNList.item(0)).getNodeValue().trim());
                    NodeList lastNameList = firstPersonElement.getElementsByTagName("telnoList");
                    Element lastNameElement = (Element) lastNameList.item(0);
                    NodeList textLNList = lastNameElement.getChildNodes();
                    System.out.println("전화번호 : "+ ((Node) textLNList.item(0)).getNodeValue().trim());
                    lastNames.add(((Node) textLNList.item(0)).getNodeValue().trim());
                }// end of if clause

            }// end of for loop with s var
            for(String firstName:firstNames)
            {
                System.out.println("firstName : "+firstName);
            }
            for(String lastName:lastNames)
            {
                System.out.println("lastName : "+lastName);
            }

        } 
        catch (SAXParseException err) 
        {
            System.out.println("** Parsing error" + ", line "+ err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());
        } 
        catch (SAXException e) 
        {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
        } 
        catch (Throwable t) 
        {
            t.printStackTrace();
        }


        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sample sheet");

        Map<String, Object[]> data = new HashMap<String, Object[]>();
        for(int i=0;i<firstNames.size();i++)
        {
        	System.out.println(firstNames);
            data.put(i+"",new Object[]{firstNames.get(i),lastNames.get(i)});
        }
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date)
                    cell.setCellValue((Date) obj);
                else if (obj instanceof Boolean)
                    cell.setCellValue((Boolean) obj);
                else if (obj instanceof String)
                    cell.setCellValue((String) obj);
                else if (obj instanceof Double)
                    cell.setCellValue((Double) obj);
            }
        }

        try {
            FileOutputStream out = new FileOutputStream(new File("C:/Users/jh/Desktop/book.xlsx"));
            workbook.write(out);
            out.close();
            System.out.println("Excel written successfully..");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
        	
        	try {
				workbook.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }

    }// end of main

}